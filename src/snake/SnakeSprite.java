
package snake;

import base.Sprite;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Vector2f;

/**
 *
 * @author christian
 */
public class SnakeSprite extends Sprite{
    LinkedList<Sprite> drawBody = new LinkedList();
    LinkedList<Sprite> body = new LinkedList();
    
    private boolean nom = false;
    private int nomCounter = 0;
    private int nomThreshold = 10;

    public SnakeSprite() {
        super(55);
        width = 10;
        height = 10;
        setMov(5,0);
        
        
    }
    
    
    
    public void nom(){
        nom = true;
        
        Vector2f bodyPart = new Vector2f();
        
        Sprite bp = new Sprite(33);
        bp.setHeight(10);
        bp.setWidth(10);
        bp.setMov(mov.x, mov.y);
        bp.setPos(pos.x, pos.y);
        bp.setColor(Color.blue);
        body.add(bp);
        bodyPart.set(pos.x, pos.y);
        
    }

    @Override
    public void update(float time) {
        
        if(nom){
            
            Sprite bp = new Sprite(33);
            bp.setHeight(10);
            bp.setWidth(10);
            bp.setMov(mov.x, mov.y);
            bp.setPos(pos.x, pos.y);
            bp.setColor(Color.blue);
            
            if(nomCounter >= nomThreshold){
                nomCounter = 0;
                drawBody.add(bp);
                nom=false;
            }
            
            body.add(bp);
            
            
            nomCounter++;
        }
        
        
        if(!body.isEmpty()){
            
            Collections.reverse(body);
            Iterator<Sprite> it = body.iterator();
            
            Sprite last = it.next();
            Sprite nextLast = last;
            
            while(it.hasNext()){
                nextLast = it.next();
                last.setPos(nextLast.getPos().x, nextLast.getPos().y);
                last.setMov(nextLast.getMov().x, nextLast.getMov().y);
                last = nextLast;
                
            }
            
            nextLast.setPos(pos.x, pos.y);
            nextLast.setMov(mov.x, mov.y);
            
            Collections.reverse(body);
        }
        
        super.update(time);
        
        
        
        
    }
    
    

    @Override
    public void draw(float interpolation) {
        
        //draw head
        super.draw(interpolation);
        
        //draw body
        for(Sprite sprite : body){
            //pos.set(sprite.getPos().x, sprite.getPos().y);
            sprite.draw(interpolation);
        }
        
    }
    
    
}
