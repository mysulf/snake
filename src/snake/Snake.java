
package snake;

import base.GameState;
import base.RenderSystemBase;
import base.StateManager;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.Timer;

/**
 *
 * @author christian
 */
public class Snake {


    private Timer timer = new Timer();

    private float time;
    private int counter;
    private int timeCounter;
    private int tickCount;


    int TICKS_PER_SECOND = 25;
    int SKIP_TICKS = 1000 / TICKS_PER_SECOND;
    int MAX_FRAMESKIP = 5;

    float next_game_tick = getTickCount();
    int loops;
    float interpolation;
    
    private RenderSystemBase renderSystem;
//    private GuiState guiState;
    private final StateManager manager;


    public Snake(){
        
        manager = new StateManager();
        
        SnakeState game = new SnakeState(manager);
        
        manager.pushState(game);
        
        this.renderSystem = new RenderSystemBase(manager);
        
        

        
    }

    private float getTickCount(){
        return ((float)(timer.getTime() * 1000));
    }

    public boolean tick(){
        
        
        
        if(Display.isCloseRequested()){
            Display.destroy();
            return false;
        }

        Timer.tick();
        loops = 0;

        while( getTickCount() > next_game_tick && loops < MAX_FRAMESKIP) {

            manager.update(getTickCount());
            

            next_game_tick += SKIP_TICKS;
            loops++;
            tickCount++;


        }
        
        interpolation = (float)
                ( (float)getTickCount() + (float) SKIP_TICKS - 
                (float)next_game_tick ) / (float)SKIP_TICKS;


        renderSystem.render(interpolation);
        
        Display.update();

        float dt = getTickCount() - time;
        time = getTickCount();
        timeCounter += dt;
        counter++;

        if(timeCounter >=1000 ){
            Display.setTitle("-SNAKE!-      "
                    + "FPS: render- " + counter + " tick- " + tickCount);
            timeCounter = 0;
            counter = 0;
            tickCount=0;
        }
        
        return true;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Snake project = new Snake();
        
        while(project.tick()){}
            
        
    }
}
