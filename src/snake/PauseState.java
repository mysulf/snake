
package snake;

import base.GameState;
import base.StateManager;
import org.lwjgl.input.Keyboard;

/**
 *
 * @author christian
 */
public class PauseState extends GameState{

    public PauseState(StateManager mngr) {
        super(mngr);
    }
    

    @Override
    public void render(float interpolation) {
        System.err.println("pause rend " + interpolation );
    }

    @Override
    public void update(float time) {
        if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)){
            parent.removeState(this);
        }
    }

    @Override
    public void enter() {
        System.out.println("Enter PauseState...");
        
    }

    @Override
    public void leave() {
        System.out.println("Leave PauseState...");
    }
    
}
