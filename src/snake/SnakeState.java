
package snake;

import base.GameState;
import base.Sprite;
import base.StateManager;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.Random;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.newdawn.slick.Color;

/**
 *
 * @author christian
 */
public class SnakeState extends GameState{

    
    SnakeSprite snake;
    
    Sprite fruit;
    
    private boolean paused = false;
    
    
    public SnakeState(StateManager mngr){
        super(mngr);
    }
    
    
    @Override
    public void render(float interpolation) {
        System.err.println("snake rend " + interpolation);
        snake.draw(interpolation);
        if(fruit!=null)
        fruit.draw(interpolation);
        
    }

    @Override
    public void enter() {
        
        snake = new SnakeSprite();
        snake.setWidth(10);
        snake.setHeight(10);
        snake.setPos(0, 0);
        
        fruit = new Sprite(7);
        fruit.setWidth(10);
        fruit.setHeight(10);
        
        
        snake.setMov(5, 0);
        
        Random rand = new Random();
        
        fruit.setPos(rand.nextInt(400), rand.nextInt(300));
        fruit.setColor(Color.red);
        
    }

    @Override
    public void update(float time) {
        snake.update(time);
        
        int col1 = 0;
        int col2 = 0;
        
        
        if(snake.getMov().x < 0){
            col1 = GLSelect(snake.getPos().x, snake.getPos().y);
            col2 = GLSelect(snake.getPos().x, snake.getPos().y+10);
        }
        else if(snake.getMov().x > 0){
            col1 = GLSelect(snake.getPos().x+10, snake.getPos().y);
            col2 = GLSelect(snake.getPos().x+10, snake.getPos().y+10);
        }
        else if(snake.getMov().y < 0){
            col1 = GLSelect(snake.getPos().x, snake.getPos().y);
            col2 = GLSelect(snake.getPos().x+10, snake.getPos().y);
        }
        else if(snake.getMov().y > 0){
            col1 = GLSelect(snake.getPos().x, snake.getPos().y+10);
            col2 = GLSelect(snake.getPos().x+10, snake.getPos().y+10);
        }
        
        
        if(col1 == 7 || col2 == 7){
             snake.nom();
             Random rand = new Random();
             fruit.setPos(rand.nextInt(400), rand.nextInt(300));
             System.out.println("Nom!");
        }
        
        else if(col1 == 33 || col2 == 33){
            System.err.println("Crash!");
           
        }
        
        if(Keyboard.isKeyDown(Keyboard.KEY_LEFT)){
            if(snake.getMov().x==0){
                snake.setMov(-5,0);
            }
        }
        
        if(Keyboard.isKeyDown(Keyboard.KEY_DOWN)){
            if(snake.getMov().y==0){
                snake.setMov(0,5);
            }
        }
        
        if(Keyboard.isKeyDown(Keyboard.KEY_UP)){
            if(snake.getMov().y==0){
                snake.setMov(0,-5);
            }
        }
        
        if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT)){
            if(snake.getMov().x==0){
                snake.setMov(5,0);
            }
        }
        if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)){
            parent.pushState(new PauseState(parent));
        }
    }
    
    public int GLSelect(float x, float y){

        IntBuffer selBuffer = ByteBuffer.allocateDirect(1024).
                order(ByteOrder.nativeOrder()).asIntBuffer();

        int buffer[] = new int[256];

        IntBuffer vpBuffer = ByteBuffer.allocateDirect(64).
                order(ByteOrder.nativeOrder()).asIntBuffer();

        int[] viewport = new int[4];

        int hits;
        GL11.glSelectBuffer(selBuffer);
        GL11.glGetInteger(GL11.GL_VIEWPORT, vpBuffer);
        vpBuffer.get(viewport);

        GL11.glRenderMode(GL11.GL_SELECT);
        GL11.glInitNames();
        GL11.glPushName(0);

        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glPushMatrix();
        GL11.glLoadIdentity();

        GLU.gluPickMatrix(x,600-y, 1.0f, 1.0f,
                IntBuffer.wrap(viewport));

        GL11.glOrtho(0.0, 800, 600, 0, 0, 10);

        GL11.glMatrixMode(GL11.GL_MODELVIEW);

        render(0f);

        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glPopMatrix();

        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        hits = GL11.glRenderMode(GL11.GL_RENDER);

        selBuffer.get(buffer);

        if (hits > 0) {
            // return the last object in the list
            return buffer[(hits-1) * 4 + 3];
        }

        return -1;
    }

    @Override
    public void leave() {
        System.out.println("Leaving SnakeState...");
    }

    
    
    
}
