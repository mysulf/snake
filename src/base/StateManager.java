
package base;

import java.util.Collections;
import java.util.Iterator;
import java.util.Stack;

/**
 *
 * @author christian
 */
public class StateManager {
    
    private Stack<GameState> stateStack = new Stack();
    
    public void pushState(GameState state){
        state.enter();
        stateStack.push(state);
    }
    
    public void removeState(GameState state){
        state.leave();
        stateStack.remove(state);
    }
    
    public void update(float time){
        stateStack.peek().update(time);
    }
    
    public void render(float interpolation){
        float temp = interpolation;
        //Collections.reverse(stateStack);
        Iterator<GameState> it = stateStack.iterator();
        
        while(it.hasNext()){
            it.next().render(temp);
            temp = 0; // removes interpolation for states not in focus
        }
    }
    
}
