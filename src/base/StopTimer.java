
package base;

import java.util.LinkedList;

/**
 *
 * @author christian
 */
public class StopTimer{
    
    private float duration = 1000;
    private float elapsed = 0;
    
    private boolean started = false;
    
    private float deltaTime = 0;
    private float oldTime = 0;
    
    private LinkedList<Listener> startedListeners = new LinkedList();
    private LinkedList<Listener> stoppedListeners = new LinkedList();

    public StopTimer(float duration) {
        this.duration = duration;
    }

    
    
    public void update(float time){
        
        deltaTime = time-oldTime;
        if(started)
            elapsed += deltaTime;
        
        if(elapsed >= duration){
            notifyTimerStopped();
        }
        
        oldTime = time;
    }
    
    public void start(){
        started=true;
    }
    
    public void stop(){
        started = false;
    }
    
    public void reset(){
        elapsed=0;
    }
    
   
    
    public void addOnTimerStartedListener(Listener list){
        this.startedListeners.add(list);
        
    }
    
    public void addOnTimerStoppedListener(Listener list){
        this.stoppedListeners.add(list);
        
    }

    private void notifyTimerStopped() {
        for(Listener list :  stoppedListeners){
            list.action(null);
        }
    }
    
    
    
    
}
