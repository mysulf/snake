
package base;

/**
 *
 * @author christian
 */
public abstract class GameState {
    protected StateManager parent;
    
    public GameState(StateManager mngr){
        this.parent = mngr;
    }
    
    public abstract void render(float interpolation);
    public abstract void update(float time);

    public abstract void enter();
    public abstract void leave();
}
