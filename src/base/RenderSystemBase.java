    
package base;

import base.GameState;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author christian
 */
public class RenderSystemBase{
    public static final int GAME_WIDTH = 800;
    public static final int GAME_HEIGHT = 600;
    
    private final StateManager game;
    
    private static int nameId = 0;
    
    
    public RenderSystemBase(StateManager g){
        this.game = g;
        create();
    }
    
    private void create(){
        System.err.println("Creating display...");
        
        boolean fullscreen = false;
        
        try {
            DisplayMode[] modes = Display.getAvailableDisplayModes();

            for (int i=0;i<modes.length;i++) {

                
                DisplayMode current = modes[i];
                
                if(current.getHeight() == RenderSystemBase.GAME_HEIGHT &&
                        current.getWidth() == RenderSystemBase.GAME_WIDTH && 
                        current.isFullscreenCapable() == true && 
                        current.getBitsPerPixel() == 32){
                    
//                   
//                System.out.println(current.getWidth() + "x" + current.getHeight() + "x" +
//                current.getBitsPerPixel() + " " + current.getFrequency() + "Hz");
                
                    Display.setDisplayMode(current);
                    Display.setFullscreen(fullscreen);
                }
                
            }
            
            
            
            Display.create();

        } catch (LWJGLException ex) {
            ex.printStackTrace();
            System.exit(0);
        }
//		GL11.glShadeModel(GL11.GL_SMOOTH);        
        GL11.glDisable(GL11.GL_DEPTH_TEST);
        GL11.glDisable(GL11.GL_LIGHTING); 
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glClearColor(0.0f, 0.0f, 0.0f, 1);  
        
//        GL11.glClearDepth(1); 
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
//        GL11.glViewport(0,0,800,600);

        GL11.glMatrixMode(GL11.GL_MODELVIEW);

        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0, RenderSystemBase.GAME_WIDTH, RenderSystemBase.GAME_HEIGHT, 0, 0, 10);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        
//        Display.update();

    } 
    
    
    public void render(float interpolation){

        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
        
        game.render(interpolation);
        
//        Display.update();
    }
    
    /**
     * Generate a unique id for use in GL picking.
     * 
     * @return A new ID
     */
    public static Integer generateGlName(){
        return nameId++;
    }
    
}
