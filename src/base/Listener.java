
package base;

/**
 *
 * @author christian
 */
public interface Listener {
    void action(Object event);
}
