
package base;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.Point;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.Color;


/**
 *
 * @author Kricke
 */
public class Sprite{

    protected int id = 0;
    protected int height=30;
    protected int width=30;

    protected Vector2f pos = new Vector2f(0,0);
    protected Vector2f center = new Vector2f(0,0);
    protected Vector2f mov = new Vector2f(0,0);
    protected Color col = Color.white;
    protected boolean destroyed = false;

    public Sprite(int id) {
        this.id = id;
    }
    
    

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
//        this.setCenter(this.center.x, this.center.y);
    }

    public Vector2f getPos() {
        return pos;
    }

    public void setPos(float posx, float posy) {
        this.pos.set(posx, posy);
        this.center.set((this.pos.getX() + this.width) / 2,
                                    (this.pos.getY() + this.height) / 2);

    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
//        this.setCenter(this.center.x, this.center.y);
    }

    public Vector2f getMov() {
        return mov;
    }

    public void setMov(float movx, float movy) {
        this.mov.set(movx, movy);
    }
    
    public Vector2f getCenter(){
        return center;
    }

    public void setCenter(float posx, float posy) {
        this.setPos(posx-this.width/2, posy-this.getHeight()/2);
    }
    
    public boolean getDestroyed(){
        return destroyed;
    }

    public Integer getId() {
        return this.id;
    }

    public  void update(float time){
        setPos(pos.x+mov.x, pos.y+mov.y);
    }

    public void draw(float interpolation) {

        GL11.glDisable (GL11.GL_TEXTURE_2D);
        
        col.bind();
        
        GL11.glPushMatrix();
        
        GL11.glTranslatef(pos.getX() + mov.getX() * interpolation, 
                pos.getY() + mov.getY() * interpolation, 0);
        //
        //GL11.glTranslatef(pos.getX(), pos.getY(), 0);
        
        GL11.glLoadName(id);
        
        GL11.glBegin(GL11.GL_QUADS);
            GL11.glVertex2f(0, 0);
            GL11.glVertex2f(0, height);
            GL11.glVertex2f(width, height);
            GL11.glVertex2f(width, 0);
        GL11.glEnd();
        
        GL11.glPopMatrix();
    }

    public void setColor(Color color) {
        col = color;
    }

}
